Fast Turtle Motors is dedicated to providing the ultimate secondhand used car buying experience. Fast Turtle Motors is your #1 source for buying quality pre-owned vehicles.

Address: 22605 La Palma Ave, Ste 505, Yorba Linda, CA 92887, USA

Phone: 714-987-1661

Website: https://www.fastturtlemotors.com
